from flask import Flask, render_template, request, redirect
"""
Projeto TCC "CONTROLE E GERENCIAMENTO DE GASTOS ELÉTRICOS"
Flask - é a microframework de desenvolvimento em python,
render_template - objeto utilizado para renderizar páginas em HTML
request - objeto utilizado para fazer solicitações à página HTML
redirect - objeto utilizado para fazer redirecionamentos à outras páginas HTML
"""

app = Flask (__name__)
@app.route ('/')
def login():
    return render_template('Login.html')

app.run(debug=True)

